const { faker } = require("@faker-js/faker/locale/es_MX");
module.exports = {
    crearPaciente: function(){
        const nombre= faker.person.firstName();
        const apellido= faker.person.lastName();
        const ci= faker.phone.number('#######');
        const email = faker.internet.email();
        const telefono= faker.phone.number('09########');
        const fecha_nacimiento= faker.date.birthdate();
        return{
            nombre,
            apellido,
            ci,
            email,
            telefono,
            fecha_nacimiento
        };
    }
}