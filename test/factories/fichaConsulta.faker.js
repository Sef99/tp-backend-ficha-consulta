const { faker } = require("@faker-js/faker/locale/es_MX");
module.exports = {
    crearFichaConsulta: function()
    {
        const fecha = faker.date.recent({days: 30});
        const motivo = faker.lorem.words({min: 5, max: 10});
        const diagnostico = faker.lorem.words({min: 10, max: 20});
        const tratamiento = faker.lorem.words({min: 10, max: 20});
        const PacienteId = faker.number.int({min:1,max:20});
        const DoctorId = faker.number.int({min:1,max:20});
        return{
            fecha,
            motivo,
            diagnostico,
            tratamiento,
            PacienteId,
            DoctorId
        };
    }
}