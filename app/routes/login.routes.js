module.exports = app => {
  const loginService = require("../controllers/login.controller.js");
  var router = require("express").Router();
  router.post("/", loginService.login);
  app.use("/api/login", router);
}