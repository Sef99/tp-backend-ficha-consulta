module.exports = app => {
    const paciente = require("../controllers/paciente.controller.js");
    var router = require("express").Router();
    router.get("/", paciente.findAll);
    router.get("/:id", paciente.findOne);
    router.post("/", paciente.create);
    router.put("/:id", paciente.update);
    router.delete("/:id", paciente.delete);
    app.use("/api/paciente" , router);
  }