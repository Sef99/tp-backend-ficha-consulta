module.exports = app => {
    const doctor = require("../controllers/doctor.controller.js");
    var router = require("express").Router();
    router.get("/", doctor.findAll);
    router.get("/:id", doctor.findOne);
    router.post("/", doctor.create);
    router.put("/:id", doctor.update);
    router.delete("/:id", doctor.delete);
    app.use("/api/doctor" , router);
  }