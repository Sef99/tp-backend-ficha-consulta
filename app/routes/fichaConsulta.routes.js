module.exports = app => {
    const fichaConsulta = require("../controllers/fichaConsulta.controller.js");
    var router = require("express").Router();
    router.get("/", fichaConsulta.findAll);
    router.post("/", fichaConsulta.create);
    app.use("/api/fichaConsulta" , router);
  }