
module.exports = (sequelize , Sequelize) => {
    const Paciente = sequelize.define("Paciente" , {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      nombre: {
        type: Sequelize.STRING,
      },
      apellido:{
        type: Sequelize.STRING
      },
      ci: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      telefono: {
        type: Sequelize.INTEGER
      },
      fecha_nacimiento: {
        type: Sequelize.DATEONLY,
        allowNull: null
      }
    },
    {
      freezeTableName: true,
    });
  
    return Paciente
  }
