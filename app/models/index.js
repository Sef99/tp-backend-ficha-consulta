const dbConfig = require("../../config/db.config.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB , dbConfig.USER , dbConfig.PASSWORD , {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  port: dbConfig.PORT,
  operatorsAliases: false,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {}
db.Sequelize = Sequelize;
db.sequelize = sequelize;


//models
db.Pacientes = require("./paciente.model.js")(sequelize , Sequelize);
db.Doctores = require("./doctor.model.js")(sequelize, Sequelize);
db.FichaConsulta = require("./fichaConsulta.model.js")(sequelize, Sequelize);

db.Pacientes.hasMany(db.FichaConsulta, {
    foreingKey: 'id'
});
db.Doctores.hasMany(db.FichaConsulta, {
    foreingKey: 'id'
});

db.FichaConsulta.belongsTo(db.Doctores);
db.FichaConsulta.belongsTo(db.Pacientes);

module.exports = db;
