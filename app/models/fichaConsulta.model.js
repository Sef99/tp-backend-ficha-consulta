
module.exports = (sequelize , Sequelize) => {
    const FichaConsulta = sequelize.define("Ficha_Consulta" , {
      id: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true
      },
      fecha: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      motivo: {
        type: Sequelize.STRING,
        allowNull: false
      },
      diagnostico: {
        type: Sequelize.STRING,
        allowNull: false
      },
      tratamiento: {
        type: Sequelize.STRING,
        allowNull: false
      },
    },
    {
      freezeTableName: true,
    });
  
    return FichaConsulta
  }
