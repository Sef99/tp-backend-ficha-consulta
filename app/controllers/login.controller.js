
const db = require("../models");
const Doctores = db.Doctores;
const Op = db.Sequelize.Op;

exports.login = async (req, res) => {
    if (!req.body.usuario) {
        res.status(400).send({
          message: "Usuario can not be empty!"
        });
        return;
      }
      if (!req.body.password) {
        res.status(400).send({
          message: "Password can not be empty!"
        });
        return;
      }
    
    const usuario = req.body.usuario;
    const password = req.body.password;

    const doctor = await Doctores.findOne({
        where: {
            usuario: usuario,
            password: password
        }
    });
    console.log(doctor);

    if (doctor === null) {
        console.log('Not Found!');
        res.status(404).send({
            message: 'Cannot find Doctor. Wrong Password or Username'
        });
    } else {
        console.log("Sending doctor");
        res.send(doctor);
    }
}