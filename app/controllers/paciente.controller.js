
const db = require("../models");
const Pacientes = db.Pacientes;
const Op = db.Sequelize.Op;

// Create and Save a new Paciente
exports.create = (req, res) => {
  // Validate request
  if (!req.body.ci) {
    res.status(400).send({
      message: "Ci can not be empty!"
    });
    return;
  }
  if (!req.body.email) {
    res.status(400).send({
      message: "Email can not be empty!"
    });
    return;
  }
  if (!req.body.fecha_nacimiento) {
    res.status(400).send({
      message: "Fecha Nacimiento can not be empty!"
    });
    return;
  }

  // Create a Paciente
  const paciente = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    ci: req.body.ci,
    email: req.body.email,
    telefono: req.body.telefono,
    fecha_nacimiento: req.body.fecha_nacimiento
  };

  // Save Paciente in the database
  Pacientes.create(paciente)
    .then(data => {
    console.log("Paciente Create!");
      res.send({
        "status" : "OK",
        "message": "Paciente created",
        "paciente" : data
      });
    })
    .catch(err => {
      console.log("ERROR!: " + err.message);
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating a Paciente."
      });
    });
};

// Retrieve all Pacientes from the database.
exports.findAll = (req, res) => {
    Pacientes.findAll()
      .then(data => {
        console.log("Get All Pacientes");
        res.send(data);
      })
      .catch(err => {
        console.log("Error: " + err.message);
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Pacientes."
        });
      });
};

// Find a single Paciente with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Pacientes.findByPk(id).then(data => {
      console.log("Find Pacientes with ID");
      res.send(data);
    }).catch(err => {
      console.log("Error: " + err.message);
      res.status(500).send({
        message: "ERROR fetching id: "+ id
      });
    });
};

// Update a Paciente by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Pacientes.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          console.log("Paciente was updated successfully");
          res.send({
            message: "Pacientes was updated successfully."
          });
        } else {
            console.log("Cannot update Paciente");
          res.send({
            message: `Cannot update Paciente with id=${id}. Maybe Paciente was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        console.log("Error: " + err.message);
        res.status(500).send({
          message: "Error updating Tutorial with id=" + id
        });
      });
};

// Delete a Paciente with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Pacientes.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
            console.log("Paciente was deleted successfully");
          res.send({
            message: "Paciente was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Paciente with id=${id}. Maybe Paciente was not found!`
          });
        }
      })
      .catch(err => {
        console.log("Error: " + err.message);
        res.status(500).send({
          message: "Could not delete Paciente with id=" + id
        });
      });
};
