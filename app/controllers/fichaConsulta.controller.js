const db = require("../models");
const FichaConsultas = db.FichaConsulta;
const Op = db.Sequelize.Op;

// Create and Save a new FichaConsulta
exports.create = (req, res) => {
  // Validate request
  if (!req.body.id_paciente) {
    res.status(400).send({
      message: "Paciente ID can not be empty!"
    });
    return;
  }
  if (!req.body.id_doctor) {
    res.status(400).send({
      message: "Doctor ID can not be empty!"
    });
    return;
  }
  if (!req.body.fecha) {
    res.status(400).send({
      message: "Fecha can not be empty!"
    });
    return;
  }
  if (!req.body.motivo) {
    res.status(400).send({
      message: "Motivo can not be empty!"
    });
    return;
  }
  if (!req.body.diagnostico) {
    res.status(400).send({
      message: "Diagnostico can not be empty!"
    });
    return;
  }
  if (!req.body.tratamiento) {
    res.status(400).send({
      message: "Tratamiento can not be empty!"
    });
    return;
  }
  

  // Create a FichaConsulta
  const FichaConsulta = {
    PacienteId : req.body.id_paciente,
    DoctorId: req.body.id_doctor,
    fecha: req.body.fecha,
    motivo: req.body.motivo,
    diagnostico: req.body.diagnostico,
    tratamiento: req.body.tratamiento
  };

  // Save FichaConsulta in the database
  FichaConsultas.create(FichaConsulta)
    .then(data => {
    console.log("FichaConsulta Create!");
      res.send({
        "status" : "OK",
        "message": "FichaConsulta created",
        "FichaConsulta" : data
      });
    })
    .catch(err => {
      console.log("ERROR!: " + err.message);
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating a FichaConsulta."
      });
    });
};

// Retrieve all FichaConsultas from the database.
exports.findAll = (req, res) => {
    FichaConsultas.findAll()
      .then(data => {
        console.log("Get All FichaConsultas");
        res.send(data);
      })
      .catch(err => {
        console.log("Error: " + err.message);
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving FichaConsultas."
        });
      });
};