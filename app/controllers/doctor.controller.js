
const db = require("../models");
const Doctores = db.Doctores;
const Op = db.Sequelize.Op;

// Create and Save a new Doctor
exports.create = (req, res) => {
  // Validate request
  if (!req.body.ci) {
    res.status(400).send({
      message: "Ci can not be empty!"
    });
    return;
  }
  if (!req.body.email) {
    res.status(400).send({
      message: "Email can not be empty!"
    });
    return;
  }
  if (!req.body.fecha_nacimiento) {
    res.status(400).send({
      message: "Fecha Nacimiento can not be empty!"
    });
    return;
  }
  if (!req.body.especialidad) {
    res.status(400).send({
      message: "Especialidad can not be empty!"
    });
    return;
  }
  if (!req.body.usuario) {
    res.status(400).send({
      message: "Usuario can not be empty!"
    });
    return;
  }
  if (!req.body.password) {
    res.status(400).send({
      message: "Password can not be empty!"
    });
    return;
  }

  // Create a Doctor
  const Doctor = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    ci: req.body.ci,
    email: req.body.email,
    telefono: req.body.telefono,
    fecha_nacimiento: req.body.fecha_nacimiento,
    especialidad: req.body.especialidad,
    usuario: req.body.usuario,
    password: req.body.password
  };

  // Save Doctor in the database
  Doctores.create(Doctor)
    .then(data => {
    console.log("Doctor Create!");
      res.send({
        "status" : "OK",
        "message": "Doctor created",
        "Doctor" : data
      });
    })
    .catch(err => {
      console.log("ERROR!: " + err.message);
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating a Doctor."
      });
    });
};

// Retrieve all Doctores from the database.
exports.findAll = (req, res) => {
    Doctores.findAll()
      .then(data => {
        console.log("Get All Doctores");
        res.send(data);
      })
      .catch(err => {
        console.log("Error: " + err.message);
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Doctores."
        });
      });
};

// Find a single Doctor with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Doctores.findByPk(id).then(data => {
      console.log("Find Doctores with ID");
      res.send(data);
    }).catch(err => {
      console.log("Error: " + err.message);
      res.status(500).send({
        message: "ERROR fetching id: "+ id
      });
    });
};

// Update a Doctor by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Doctores.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          console.log("Doctor was updated successfully");
          res.send({
            message: "Doctores was updated successfully."
          });
        } else {
            console.log("Cannot update Doctor");
          res.send({
            message: `Cannot update Doctor with id=${id}. Maybe Doctor was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        console.log("Error: " + err.message);
        res.status(500).send({
          message: "Error updating Tutorial with id=" + id
        });
      });
};

// Delete a Doctor with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Doctores.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
            console.log("Doctor was deleted successfully");
          res.send({
            message: "Doctor was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Doctor with id=${id}. Maybe Doctor was not found!`
          });
        }
      })
      .catch(err => {
        console.log("Error: " + err.message);
        res.status(500).send({
          message: "Could not delete Doctor with id=" + id
        });
      });
};
